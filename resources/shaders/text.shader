#shader vertex
#version 330 core

layout (location = 0) in vec2 i_position;
layout (location = 1) in vec2 i_uv;
layout (location = 2) in vec4 i_color;

uniform mat4 u_projectionMatrix;

out vec2 v_uv;
out vec4 v_color;

void main()
{
    v_uv = i_uv;
    v_color = i_color;
    gl_Position = u_projectionMatrix * vec4(i_position.xy,0,1);
}

#shader fragment
#version 330 core

in vec2 v_uv;
in vec4 v_color;

uniform sampler2D tex0;

layout (location = 0) out vec4 outputColor;

void main()
{
    outputColor = v_color * texture(tex0, v_uv.st);
}
