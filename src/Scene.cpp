
#include "Scene.hpp"

#include <glm/glm.hpp>

#include "AssetDatabase.hpp"
#include "MainWindow.hpp"
#include "scenegraph/nodes/MeshRenderer.hpp"
#include "scenegraph/nodes/PlayerMovement.hpp"
#include "scenegraph/nodes/SkyBox.hpp"
#include "scenegraph/nodes/Torque.hpp"

namespace Sandbox {

static void GenCubes(std::shared_ptr<Node> parent) {
    int count = 8;
    float gap = 8;

    float offset = -(count - 1) / 2.0f;

    for (int x = 0; x < count; x++) {
        for (int y = 0; y < count; y++) {
            for (int z = 0; z < count; z++) {
                auto cubeObj = std::make_shared<MeshRenderer>(
                    ASSETS.shaders["diffuse"], ASSETS.meshes["cube"],
                    ASSETS.textures["grass0"]);
                cubeObj->GetTransformMut().SetPosition(
                    glm::vec3((offset + x) * gap, (offset + y) * gap,
                              (offset + z) * gap));
                parent->AddChild(std::move(cubeObj));
            }
        }
    }
}

Scene::Scene(MainWindow &window)
    : window(window), root(std::make_shared<Node>()),
      camera(std::make_shared<Camera>(60.0f, window.ViewportAspect(), 0.01f,
                                      1000.0f)) {
    // Load Assets
    ASSETS.shaders["unlit"] = std::make_shared<Shader>("default.shader");
    ASSETS.shaders["diffuse"] = std::make_shared<Shader>("diffuse.shader");
    ASSETS.shaders["skybox"] = std::make_shared<Shader>("skygradient.shader");

    ASSETS.textures["grid"] = std::make_shared<Texture>("grid.png", false);
    ASSETS.textures["grass"] = std::make_shared<Texture>("grass.png", false);
    ASSETS.textures["grass0"] = std::make_shared<Texture>("grass0.png", false);

    ASSETS.meshes["triangle"] = std::make_shared<Mesh>(MeshGen::triangle());
    ASSETS.meshes["quad"] = std::make_shared<Mesh>(MeshGen::quad());
    ASSETS.meshes["cube"] = std::make_shared<Mesh>(MeshGen::cube());

    // Initialize Scene Graph
    auto grassRenderer = std::make_shared<MeshRenderer>(
        ASSETS.shaders["diffuse"], ASSETS.meshes["cube"],
        ASSETS.textures["grass"]);
    grassRenderer->SetTransform(
        WorldTransform(glm::vec3(), glm::vec3(0, glm::radians(45.0f), 0)));

    auto planeRenderer = std::make_shared<MeshRenderer>(
        ASSETS.shaders["diffuse"], ASSETS.meshes["quad"],
        ASSETS.textures["grid"]);
    planeRenderer->SetTransform(WorldTransform(
        glm::vec3(0, -10, 0), glm::vec3(-glm::half_pi<float>(), 0, 0),
        glm::vec3(40, 40, 1)));

    auto skybox = std::make_shared<SkyBox>(glm::vec4(0.74f, 0.83f, 0.92f, 1.0f),
                                           glm::vec4(0.22f, 0.42f, 0.71f, 1.0f),
                                           ASSETS.shaders["skybox"]);

    auto movement = std::make_shared<PlayerMovement>(10.0f, 0.2f);
    movement->SetTransform(WorldTransform(glm::vec3(0, 0, 10)));

    auto cubes = std::make_shared<Torque>(glm::vec3(1.0f, 0.5f, 0.25f));
    cubes->GetTransformMut().SetPosition(glm::vec3(0, 20, 0));
    cubes->GetTransformMut().SetScale(glm::vec3(0.2f));
    GenCubes(cubes);

    camera->AddChild(skybox);
    movement->AddChild(camera);
    root->AddChild(movement);
    root->AddChild(grassRenderer);
    root->AddChild(planeRenderer);
    root->AddChild(cubes);

    initCallbacks();
}

Scene::~Scene() {}

void Scene::initCallbacks() {
    window.UpdateCallback([this](double delta) { this->Update(delta); });
    window.RenderCallback([this](double delta) { this->Render(delta); });
    window.EventCallback([this](Event &event) { this->OnEvent(event); });
}

void Scene::Update(float deltatime) { root->Update(deltatime); }

void Scene::Render(float deltatime) const {
    // Camera view and perspective
    glm::mat4 view = camera->View();
    glm::mat4 projection = camera->Projection();

    root->Render(deltatime, view, projection);
}

void Scene::OnEvent(Event &event) {
    if (event.GetType() == EventType::Keyboard) {
        OnKey((KeyboardEvent &)event);
    } else if (event.GetType() == EventType::MouseButton) {
        OnMouseButton((MouseButtonEvent &)event);
    }
    if (!event.handled)
        root->OnEvent(event);
}

void Scene::OnKey(KeyboardEvent &event) {
    if (event.GetKeyCode() == GLFW_KEY_ESCAPE) {
        window.Close();
        event.handled = true;
    } else if (event.GetKeyCode() == GLFW_KEY_LEFT_ALT) {
        window.CursorMode(event.GetAction() == KeyAction::KeyReleased);
    }
}

void Scene::OnMouseButton(MouseButtonEvent &event) {
    if (event.GetButton() == 1) {
        window.CursorMode(event.GetAction() == KeyAction::KeyReleased);
    }
}

} // namespace Sandbox
