#pragma once

#include <functional>
#include <iostream>
#include <memory>

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "Event.hpp"

namespace Sandbox {
class MainWindow;
}

#include "rendering/ui/UI.hpp"

namespace Sandbox {

const int WINDOW_WIDTH = 800;
const int WINDOW_HEIGHT = 600;

/**
 * @brief The function signature for the render callback.
 *
 * This function is called every tick with the current frame time.
 *
 * @param[in] deltatime The time until the last render call in seconds.
 */
using RenderFn = std::function<void(float)>;

/**
 * @brief The function signature for the update callback.
 *
 * This function is called every tick with the current frame time.
 *
 * @param[in] deltatime The time until the last render call in seconds.
 */
using UpdateFn = std::function<void(float)>;

/**
 * @brief      Wrapper for GLFW Windows
 */
class MainWindow {
  private:
    /**
     * @brief Initializes the main window.
     *
     * Initializes the main window with GLFW and the OpenGL context.
     */
    GLFWwindow *window;
    bool closing;
    bool enabledCursor;
    float updateTime;
    float renderTime;

    std::unique_ptr<UI> ui;

    // EventHandler
    RenderFn renderCallback;
    UpdateFn updateCallback;
    OnEventFn eventCallback;

    void InitCallbacks();

    void Update();
    void Render();

    void OnEvent(Event &event);

  public:
    /**
     * @brief Initializes the window library.
     *
     * This has to be called befor the first window is created.
     *
     * @return Whether the initialization was successful.
     */
    static bool Initialize() {
        if (!glfwInit()) {
            std::cerr << "Unable to initialize GLFW!" << std::endl;
            return false;
        }

        glfwSetErrorCallback([](int errorCode, const char *error) {
            std::cerr << "[ GLFW Error " << errorCode << " ]: " << error
                      << std::endl;
        });
        return true;
    }

    /**
     * @brief Terminates the window library and cleans up its resources.
     */
    static void Terminate() { glfwTerminate(); }

    /**
     * @brief Construct a new window.
     */
    MainWindow();
    ~MainWindow();

    MainWindow(MainWindow const &) = delete;
    void operator=(MainWindow const &) = delete;

    /**
     * @brief      Returns a pointer to the underlaying glfw window instance.
     *
     * @return     The glfw window pointer.
     */
    inline GLFWwindow *Glfw() const { return window; }

    /**
     * @brief      Resizes the main window.
     *
     * @param[in]  width   The width
     * @param[in]  height  The height
     */
    inline void Resize(int width, int height) {
        glfwSetWindowSize(window, width, height);
    }

    /**
     * @brief Enables or disables the mouse cursor.
     *
     * @param[in] enable If the cursor should be enabled.
     */
    inline void CursorMode(bool enable) {
        enabledCursor = enable;
        glfwSetInputMode(window, GLFW_CURSOR,
                         enable ? GLFW_CURSOR_NORMAL : GLFW_CURSOR_DISABLED);
    }

    /**
     * @brief Returns if the mouse curser is enabled.
     */
    inline bool IsCursorEnabled() const {
        return glfwGetInputMode(window, GLFW_CURSOR) == GLFW_CURSOR_NORMAL;
    }

    /**
     * @brief Returns if the window is focused.
     */
    inline bool IsFocused() const {
        return glfwGetWindowAttrib(window, GLFW_FOCUSED) != 0;
    }

    /**
     * @brief Updates the mouse cursor position.
     */
    inline void SetCursor(glm::vec2 position) {
        glfwSetCursorPos(window, (double)position.x, (double)position.y);
    }

    /**
     * @brief Returns the clipboard text.
     */
    inline const char *GetClipboardText() const {
        return glfwGetClipboardString(window);
    }

    /**
     * @brief Sets the clipboard text.
     */
    inline void SetClipboardText(const char *text) {
        glfwSetClipboardString(window, text);
    }

    /**
     * @brief Sets the update callback which is called every frame.
     *
     * @param[in] callback that should be executed.
     */
    inline void UpdateCallback(UpdateFn callback) { updateCallback = callback; }

    /**
     * @brief Sets the rendering callback which is called every frame.
     *
     * @param[in] callback that should be executed.
     */
    inline void RenderCallback(RenderFn callback) { renderCallback = callback; }

    /**
     * @brief Sets the event handler callback.
     *
     * @param[in] callback that handles events.
     */
    inline void EventCallback(OnEventFn callback) { eventCallback = callback; }

    /**
     * @brief Displays the window until it closes.
     *
     * Displays the window and blocks this thread until the window is closed.
     */
    void showAndWait();

    /**
     * @brief Returns whether the window is closing.
     */
    inline bool ShouldClose() const {
        return glfwWindowShouldClose(window) || closing;
    }

    /**
     * @brief Closes the window on the next tick.
     */
    void Close();

    /**
     * @brief Returns the size of the Framebuffer/Viewport.
     *
     * Returns the size of the Framebuffer/Viewport which can be different from
     * the window size on HDPI displays.
     *
     * @return int vector of the width and hight
     */
    glm::vec2 ViewportSize() const;

    /**
     * @brief Returns the aspect ratio of the Framebuffer/Viewport.
     */
    float ViewportAspect() const;

    /**
     * @brief      Returns the actual window size.
     *
     * Returns the size of this window which can be different from
     * the Framebuffer size on HDPI displays.
     *
     * @return     The window size.
     */
    glm::vec2 WindowSize() const;
};

} // namespace Sandbox
