#include "VertexBufferLayout.hpp"

#include <iostream>

namespace Sandbox {

void VertexBufferLayout::PushFloat(unsigned int count) {
    VertexBufferElement element{GL_FLOAT, count, GL_FALSE};
    elements.push_back(element);
    stride += element.Size();
}

void VertexBufferLayout::PushUint(unsigned int count) {
    VertexBufferElement element{GL_UNSIGNED_INT, count, GL_FALSE};
    elements.push_back(element);
    stride += element.Size();
}

void VertexBufferLayout::PushByte(unsigned int count) {
    VertexBufferElement element{GL_UNSIGNED_BYTE, count, GL_TRUE};
    elements.push_back(element);
    stride += element.Size();
}

} // namespace Sandbox
