#pragma once

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <string>
#include <unordered_map>

namespace Sandbox {

#define SHADER_PATH "/resources/shaders/"

/**
 * @brief      Simple shader program for vertex and fragment shaders.
 */
class Shader {
    /**
     * @brief      The raw source code of the vertex and fragment shaders.
     */
    struct ShaderSource {
        std::string vertex;
        std::string fragment;
    };

    unsigned int program;
    std::unordered_map<std::string, int> uniform_locations;

    static ShaderSource parseShader(const std::string &filename);
    static unsigned int compileShader(unsigned int type,
                                      const std::string &raw);
    static unsigned int createProgram(unsigned int vert, unsigned int frag);

    inline int uniformId(const std::string &location) const {
        return uniform_locations.at(location);
    }

  public:
    /**
     * @brief      Loads and compiles a new shader.
     *
     * @param[in]  filename  The filename of the shader source which contains
     *                       both the vertex and the fragment shader.
     */
    explicit Shader(const std::string &filename);
    ~Shader();

    Shader(Shader const &) = delete;
    void operator=(Shader const &) = delete;

    /**
     * @brief      Installs the shader program as part of current rendering
     * state.
     */
    void Bind() const;

    /**
     * @brief      Installs the shader program as part of current rendering
     * state.
     *
     * @param[in]  projection  The projection matrix of the camera.
     */
    void Bind(const glm::mat4 &projection) const;

    /**
     * @brief      Installs the shader program as part of current rendering
     * state.
     *
     * @param[in]  projection  The projection matrix of the camera.
     * @param[in]  view        The view matrix of the camera.
     */
    void Bind(const glm::mat4 &projection, const glm::mat4 &view) const;

    /**
     * @brief      Installs the shader program as part of current rendering
     * state.
     *
     * @param[in]  projection  The projection matrix of the camera.
     * @param[in]  view        The view matrix of the camera.
     * @param[in]  model       The model matrix of the object that should be
     * rendered.
     */
    void Bind(const glm::mat4 &projection, const glm::mat4 &view,
              const glm::mat4 &model) const;

    /**
     * @brief      Installs the shader program as part of current rendering
     * state.
     *
     * @param[in]  projection  The projection matrix of the camera.
     * @param[in]  view        The view matrix of the camera.
     * @param[in]  model       The model matrix of the object that should be
     * rendered.
     * @param[in]  normal      The normal matrix of the object that should be
     * rendered.
     */
    void Bind(const glm::mat4 &projection, const glm::mat4 &view,
              const glm::mat4 &model, const glm::mat3 &normal) const;

    /**
     * @brief      Removes the shader from the current rendering state.
     */
    void Unbind() const;

    /**
     * @brief      Passes the data for the diffuse shader into this shader.
     *
     * This has to be called after the 'bind' call during the rendering.
     *
     * @param[in]  dir             The dir
     * @param[in]  color           The color
     * @param[in]  lightIntensity  The light intensity
     */
    void Diffuse(const glm::vec3 &dir, const glm::vec3 &color,
                 float lightIntensity) const;

    /**
     * @brief      Passes a value to the shader.
     *
     * This has to be called after the bind() during the rendering.
     *
     * @param[in]  location  The name of the shader uniform variable.
     * @param[in]  val       The value that should be passed to the shader.
     */
    void Uniform(const std::string &location, int val) const;

    /**
     * @brief      Passes a value to the shader.
     *
     * This has to be called after the bind() during the rendering.
     *
     * @param[in]  location  The name of the shader uniform variable.
     * @param[in]  val       The value that should be passed to the shader.
     */
    void Uniform(const std::string &location, float val) const;

    /**
     * @brief      Passes a value to the shader.
     *
     * This has to be called after the 'bind' call during the rendering.
     *
     * @param[in]  location  The name of the shader uniform variable.
     * @param[in]  val       The value that should be passed to the shader.
     */
    void Uniform(const std::string &location, const glm::vec2 &val) const;

    /**
     * @brief      Passes a value to the shader.
     *
     * This has to be called after the 'bind' call during the rendering.
     *
     * @param[in]  location  The name of the shader uniform variable.
     * @param[in]  val       The value that should be passed to the shader.
     */
    void Uniform(const std::string &location, const glm::vec3 &val) const;

    /**
     * @brief      Passes a value to the shader.
     *
     * This has to be called after the 'bind' call during the rendering.
     *
     * @param[in]  location  The name of the shader uniform variable.
     * @param[in]  val       The value that should be passed to the shader.
     */
    void Uniform(const std::string &location, const glm::vec4 &val) const;

    /**
     * @brief      Passes a value to the shader.
     *
     * This has to be called after the 'bind' call during the rendering.
     *
     * @param[in]  location  The name of the shader uniform variable.
     * @param[in]  val       The value that should be passed to the shader.
     */
    void Uniform(const std::string &location, const glm::mat3 &val) const;

    /**
     * @brief      Passes a value to the shader.
     *
     * This has to be called after the 'bind' call during the rendering.
     *
     * @param[in]  location  The name of the shader uniform variable.
     * @param[in]  val       The value that should be passed to the shader.
     */
    void Uniform(const std::string &location, const glm::mat4 &val) const;
};

} // namespace Sandbox
