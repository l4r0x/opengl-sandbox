#pragma once

namespace Sandbox {

/**
 * @brief      The vertex buffer for a vertex array.
 */
class VertexBuffer {
    unsigned int rendererID;

  public:
    /**
     * @brief      Creates a new vertex buffer.
     */
    VertexBuffer();

    /**
     * @brief      Creates a new vertex buffer.
     *
     * @param[in]  data  The vertex data to be stored in this buffer.
     * @param[in]  size  The total size of this buffer.
     */
    VertexBuffer(const void *data, unsigned int size);
    ~VertexBuffer();

    VertexBuffer(VertexBuffer const &) = delete;
    void operator=(VertexBuffer const &) = delete;

    /**
     * @brief      Updates the buffer data.
     *
     * @param[in]  data   The indices to be stored in this buffer.
     * @param[in]  count  The number of indices.
     */
    void UpdateData(const void *data, unsigned int size);

    /**
     * @brief      Binds this buffer.
     */
    void Bind() const;

    /**
     * @brief      Unbinds this buffer.
     */
    void Unbind() const;
};

} // namespace Sandbox
