#pragma once

#include <glad/glad.h>
#include <string>

namespace Sandbox {

#define TEXTURE_PATH "/resources/textures/"

struct Image {
    unsigned char *data;
    int width, height;
    int channels;
};

/**
 * @brief      Class for loading and handling textures.
 */
class Texture {
    unsigned int id;

    Image loadImage(std::string filepath);

    void init(Image &image, bool alpha);

  public:
    /**
     * @brief      Creates a new texture object from the given image.
     *
     * The image should have the RGBA format.
     *
     * @param[in]  image  The image data.
     */
    Texture(Image &image, bool alpha = false);

    /**
     * @brief      Creates a new texture object from the given image asset.
     *
     * The image should have the RGBA format.
     *
     * @param[in]  filepath  The file path of the source image.
     */
    Texture(std::string filepath, bool alpha = false);
    ~Texture();

    inline unsigned int Id() const {
        return id;
    }

    /**
     * @brief      Binds this texture to a texturing target.
     */
    void Bind() const;

    /**
     * @brief      Unbinds the texture from the current rendering state.
     */
    void Unbind() const;
};

} // namespace Sandbox
