#pragma once

namespace Sandbox {

/**
 * @brief      The index buffers for vertex arrays.
 */
class IndexBuffer {
    unsigned int rendererID;

  public:
    /**
     * @brief      Creates a new index buffer.
     */
    IndexBuffer();
    /**
     * @brief      Creates a new index buffer.
     *
     * @param[in]  data   The indices to be stored in this buffer.
     * @param[in]  count  The number of indices.
     */
    IndexBuffer(const unsigned int *data, unsigned int count);
    /**
     * @brief      Creates a new index buffer.
     *
     * @param[in]  data   The indices to be stored in this buffer.
     * @param[in]  count  The number of indices.
     */
    IndexBuffer(const unsigned short *data, unsigned int count);
    ~IndexBuffer();

    IndexBuffer(IndexBuffer const &) = delete;
    void operator=(IndexBuffer const &) = delete;

    /**
     * @brief      Updates the buffer data.
     *
     * @param[in]  data   The indices to be stored in this buffer.
     * @param[in]  count  The number of indices.
     */
    void UpdateData(const unsigned int *data, unsigned int count);
    /**
     * @brief      Updates the buffer data.
     *
     * @param[in]  data   The indices to be stored in this buffer.
     * @param[in]  count  The number of indices.
     */
    void UpdateData(const unsigned short *data, unsigned int count);

    /**
     * @brief      Binds this buffer.
     */
    void Bind() const;

    /**
     * @brief      Unbinds this buffer.
     */
    void Unbind() const;
};

} // namespace Sandbox
