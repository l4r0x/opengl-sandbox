#include "VertexBuffer.hpp"

#include <glad/glad.h>

#include "Debug.hpp"

namespace Sandbox {

VertexBuffer::VertexBuffer() { GLCall(glGenBuffers(1, &rendererID)); }

VertexBuffer::VertexBuffer(const void *data, unsigned int size)
    : VertexBuffer() {
    UpdateData(data, size);
}

VertexBuffer::~VertexBuffer() { GLCall(glDeleteBuffers(1, &rendererID)); }

void VertexBuffer::UpdateData(const void *data, unsigned int size) {
    Bind();
    GLCall(glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW));
}

void VertexBuffer::Bind() const {
    GLCall(glBindBuffer(GL_ARRAY_BUFFER, rendererID));
}

void VertexBuffer::Unbind() const { GLCall(glBindBuffer(GL_ARRAY_BUFFER, 0)); }

} // namespace Sandbox
