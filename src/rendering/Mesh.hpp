#pragma once

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <vector>

#include "VertexArray.hpp"

namespace Sandbox {

/**
 * Vertex that contains the position, normal direction and texture coordinates.
 */
struct Vertex {
    glm::vec3 position;
    glm::vec3 normal;
    glm::vec2 uv;
};

/**
 * Collection of basic meshes.
 */
namespace MeshGen {
struct MeshResources {
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;

    MeshResources(std::vector<Vertex> vertices,
                  std::vector<unsigned int> indices)
        : vertices(vertices), indices(indices) {}
};

MeshResources triangle();
MeshResources quad();
MeshResources cube();
} // namespace MeshGen

/**
 * @brief   The mesh class represents the structure of a 3D object inside the
 * world.
 *
 * This is a simple demo class for vertex array that consists of a single vertex
 * buffer.
 */
class Mesh {
    unsigned int indexCount;

    VertexArray va;

  public:
    /**
     * @brief      Initializes a new mesh object.
     *
     * @param[in]  vertices  The vertices of this mesh.
     * @param[in]  indices   The indices which defines the resulting triangles.
     */
    Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices);

    /**
     * @brief      Initializes a new mesh object
     *
     * @param[in]  data  The data which consists of vertices and indices.
     */
    explicit Mesh(MeshGen::MeshResources data) : Mesh(data.vertices, data.indices) {}
    ~Mesh();

    Mesh(Mesh const &) = delete;
    void operator=(Mesh const &) = delete;

    /**
     * @brief      Draws the mesh to the opengl scene.
     *
     * Shaders have to be bound right before this draw call.
     */
    void Draw() const;
};

} // namespace Sandbox
