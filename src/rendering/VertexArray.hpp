#pragma once

#include "VertexBuffer.hpp"
#include "VertexBufferLayout.hpp"

namespace Sandbox {

/**
 * @brief      Vertex array which represents the mesh of a 3D object.
 */
class VertexArray {
    unsigned int rendererID;

  public:
    /**
     * @brief      Creates a new vertex array
     */
    VertexArray();
    ~VertexArray();

    VertexArray(VertexArray const &) = delete;
    void operator=(VertexArray const &) = delete;

    /**
     * @brief      Adds a vertex buffer to this vertex array.
     *
     * A vertex array can consist of multiple vertex buffers that are
     * addressed by a single index buffer.
     *
     * @param[in]  vb      The vertex buffer.
     * @param[in]  layout  The layout of the data inside this buffer.
     */
    void AddBuffer(const VertexBuffer &vb, const VertexBufferLayout &layout);

    /**
     * @brief      Binds the vertex array.
     */
    void Bind() const;

    /**
     * @brief      Unbinds the vertex array.
     */
    void Unbind() const;
};

} // namespace Sandbox
