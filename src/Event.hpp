#pragma once

#include <functional>
#include <glm/glm.hpp>

namespace Sandbox {

/**
 * @brief      The event type for distinguishing events during runtime.
 */
enum EventType {
    None = 0,
    WindowClose,
    WindowResize,
    Keyboard,
    KeyChar,
    MouseButton,
    MouseScroll,
    MouseMove,
};

/**
 * @brief      The base event class defining the necessary abstract functions.
 */
class Event {
  public:
    /**
     * If the event is handled and should not propagate further.
     */
    bool handled = false;

    /**
     * @return     The event type to distinguish events on runtime.
     */
    virtual EventType GetType() const = 0;

    /**
     * @return     The name of the event for debugging and error tracking.
     */
    virtual const char *GetName() const = 0;
};

/**
 * Event callback.
 */
using OnEventFn = std::function<void(Event &)>;

/* Window Events */

/**
 * @brief      The window close event is issued when the user closes the window.
 */
class WindowCloseEvent : public Event {
  public:
    /**
     * @brief      Creates the event.
     */
    WindowCloseEvent() {}

    virtual EventType GetType() const { return EventType::WindowClose; };
    virtual const char *GetName() const { return "WindowCloseEvent"; };
};

/**
 * @brief      The window resize even is issued when the window size changes.
 */
class WindowResizeEvent : public Event {
  protected:
    unsigned int width;
    unsigned int height;

  public:
    /**
     * @brief      Creates the event.
     *
     * @param[in]  width   The new window width
     * @param[in]  height  The new window height
     */
    WindowResizeEvent(unsigned int width, unsigned int height)
        : width(width), height(height) {}

    virtual EventType GetType() const { return EventType::WindowResize; };
    virtual const char *GetName() const { return "WindowResizeEvent"; };

    /**
     * @return     The new window width.
     */
    inline unsigned int GetWidth() const { return width; }

    /**
     * @return     The new window height.
     */
    inline unsigned int GetHeight() const { return height; }
};

/* Keyboard Events */

/**
 * @brief      Defines keyboard and mouse button actions.
 */
enum KeyAction {
    KeyPressed,
    KeyReleased,
    KeyRepeated,
};

/**
 * @brief      The keyboard event is issued when a button is pressed,
 *             repeated or released.
 */
class KeyboardEvent : public Event {
  protected:
    int keyCode;
    KeyAction action;

  public:
    /**
     * @brief      Creates the event.
     *
     * @param[in]  keyCode  The key code of the keyboard button
     * @param[in]  action   The action (press, repeat, release)
     */
    KeyboardEvent(int keyCode, KeyAction action)
        : keyCode(keyCode), action(action) {}

    virtual EventType GetType() const { return EventType::Keyboard; };
    virtual const char *GetName() const { return "KeyboardEvent"; };

    /**
     * @return     The key code of the keyboard button.
     */
    inline int GetKeyCode() const { return keyCode; }

    /**
     * @return     The action (press, repeat, release).
     */
    inline KeyAction GetAction() const { return action; }
};

/**
 * @brief      The keyboard character event is issued when the user presses
 *             a valid character key.
 */
class KeyCharEvent : public Event {
  protected:
    unsigned int character;

  public:
    /**
     * @param[in]  character  The character which is pressed
     */
    explicit KeyCharEvent(unsigned int character) : character(character) {}

    virtual EventType GetType() const { return EventType::KeyChar; };
    virtual const char *GetName() const { return "KeyCharEvent"; };

    /**
     * @return     The character which is pressed.
     */
    inline unsigned int GetCharacter() const { return character; }
};

/* Mouse Events */

/**
 * @brief      The mouse button event is issued when a mouse button is
 *             pressed or released.
 */
class MouseButtonEvent : public Event {
  protected:
    int button;
    KeyAction action;

  public:
    /**
     * @brief      Creates the event.
     *
     * @param[in]  button  The mouse button
     * @param[in]  action  The action (press or release)
     */
    MouseButtonEvent(int button, KeyAction action)
        : button(button), action(action) {}

    virtual EventType GetType() const { return EventType::MouseButton; };
    virtual const char *GetName() const { return "MouseButtonEvent"; };

    /**
     * @return     The mouse button.
     */
    inline int GetButton() const { return button; }

    /**
     * @return     The action (press or release).
     */
    inline KeyAction GetAction() const { return action; }
};

/**
 * @brief      The mouse scroll event is issued when the user scrolls with the
 *             mouse wheel or trackpad.
 */
class MouseScrollEvent : public Event {
  protected:
    glm::vec2 offset;

  public:
    /**
     * @brief      Creates the event
     *
     * @param[in]  offset  The scroll offset in x and y direction
     */
    explicit MouseScrollEvent(glm::vec2 offset) : offset(offset) {}

    virtual EventType GetType() const { return EventType::MouseScroll; };
    virtual const char *GetName() const {
        return "MouseMouseScrollScrollEvent";
    };

    /**
     * @return     The scroll offset in x and y direction.
     */
    inline glm::vec2 GetOffset() const { return offset; }
};

/**
 * @brief      The mouse move event is issued when the user moves the mouse
 *             pointer across the window.
 */
class MouseMoveEvent : public Event {
  protected:
    glm::vec2 position;

  public:
    /**
     * @brief      Creates the event
     *
     * @param[in]  position  The new mouse position on the screen.
     */
    explicit MouseMoveEvent(glm::vec2 position) : position(position) {}

    virtual EventType GetType() const { return EventType::MouseMove; };
    virtual const char *GetName() const { return "MouseMoveEvent"; };

    /**
     * @return     The new mouse position on the screen.
     */
    inline glm::vec2 GetPosition() const { return position; }
};

} // namespace Sandbox
