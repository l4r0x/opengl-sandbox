#pragma once

#include <glm/glm.hpp>

namespace Sandbox {

/**
 * @brief      Represents a 3D transformation.
 */
struct WorldTransform {
  public:
    /**
     * @brief      Creates a new transform.
     *
     * @param[in]  translation  The local position as x, y and z coordinates.
     * @param[in]  rotation     The local rotation as euler angles.
     * @param[in]  scale        The local scale as x, y and z scalars.
     */
    WorldTransform(const glm::vec3 &position = glm::vec3(),
                   const glm::vec3 &rotation = glm::vec3(),
                   const glm::vec3 &scale = glm::vec3(1));

    /** @return      The local position as x, y and z coordinates. */
    inline const glm::vec3 &Position() const { return localPosition; }

    /** @return      The local rotation as euler angles. */
    inline const glm::vec3 &Rotation() const { return localRotation; }

    /** @return      The local scale as x, y and z scalars. */
    inline const glm::vec3 &Scale() const { return localScale; }

    /** @return      The model matrix of this object. */
    inline const glm::mat4 &Model() const { return model; }

    /** @param[in]   The local position as x, y and z coordinates. */
    inline void SetPosition(const glm::vec3 &position) {
        localPosition = position;
    }

    /** @param[in]   The local rotation as euler angles. */
    inline void SetRotation(const glm::vec3 &rotation) {
        localRotation = rotation;
    }

    /** @param[in]   The local scale as x, y and z scalars. */
    inline void SetScale(const glm::vec3 &scale) { localScale = scale; }

    /**
     * @brief      Updates the transformation matrix.
     *
     * @param[in]  parent  The transformation (model) matrix of the parent
     * object.
     */
    void UpdateGlobal(const glm::mat4 &parent = glm::mat4(1));

    /**
     * @brief      Calculates the view matrix for the camera with this
     * transformation.
     *
     * @return     A view matrix which represents the camera position and
     * rotation.
     */
    glm::mat4 ViewMatrix() const;

    /**
     * @brief      Calculates the global normal matrix which is used by shader.
     *
     * @param[in]  view   The view matrix of the current camera.
     * @param[in]  model  The global transformation of this object.
     *
     * @return     A matrix for calculating the normal directions of meshes.
     */
    glm::mat3 NormalMatrix(const glm::mat4 &view) const;

  private:
    /** @brief      The local position as x, y and z coordinates. */
    glm::vec3 localPosition;

    /** @brief      The local rotation as euler angles. */
    glm::vec3 localRotation;

    /** @brief      The local scale as x, y and z scalars. */
    glm::vec3 localScale;

    /**
     * @brief      The global transformation (model matrix) of this object.
     *
     * The global transformation of an object is calculated of its local
     * transformation and the transformation of its parent.
     */
    glm::mat4 model;

    /**
     * @brief      Combines the translation, rotation and scale in a single
     * transformation matrix.
     *
     * @param[in] p  The transformation or position vector.
     * @param[in] r  The rotation as euler angles (radian).
     * @param[in] s  The scale vector.
     *
     * @return     The transformation matrix.
     */
    glm::mat4 CalcTransform(const glm::vec3 &p, const glm::vec3 &r,
                            const glm::vec3 &s) const;
};

} // namespace Sandbox
