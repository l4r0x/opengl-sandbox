
#include "SkyBox.hpp"

#include "AssetDatabase.hpp"
#include <glad/glad.h>

namespace Sandbox {

SkyBox::SkyBox(glm::vec4 horizonColor, glm::vec4 skyColor,
               std::shared_ptr<Shader> shader)
    : Node(), horizonColor(horizonColor), skyColor(skyColor), shader(shader),
      mesh(ASSETS.meshes["cube"]) {}

void SkyBox::Render(float delta, const glm::mat4 &view,
                    const glm::mat4 &projection) const {
    glDepthMask(GL_FALSE);
    glCullFace(GL_FRONT);
    shader->Bind(projection, view);
    shader->Uniform("u_skyHorizon", horizonColor);
    shader->Uniform("u_skyTop", skyColor);
    mesh->Draw();
    shader->Unbind();
    glCullFace(GL_BACK);
    glDepthMask(GL_TRUE);
}

} // namespace Sandbox
