#pragma once

#include <glm/glm.hpp>
#include <memory>

#include "rendering/Mesh.hpp"
#include "rendering/Shader.hpp"
#include "rendering/Texture.hpp"
#include "Spatial.hpp"

namespace Sandbox {

/**
 * @brief      Handles the rendering of meshes with shaders and textures.
 */
class MeshRenderer : public Spatial {
  private:
    std::shared_ptr<Shader> shader;
    std::shared_ptr<Mesh> mesh;
    std::shared_ptr<Texture> texture;

  public:
    /**
     * @brief      Creates a new Mesh Renderer component with the given shader,
     *             mesh and texture.
     *
     * @param[in]  shader   A reference to the shader asset.
     * @param[in]  mesh     A reference to the mesh asset.
     * @param[in]  texture  A reference to the texture asset.
     */
    MeshRenderer(std::shared_ptr<Shader> shader, std::shared_ptr<Mesh> mesh,
                 std::shared_ptr<Texture> texture);

    MeshRenderer(MeshRenderer const &) = delete;
    void operator=(MeshRenderer const &) = delete;

    virtual void Render(float delta, const glm::mat4 &view,
                        const glm::mat4 &projection) const;
};

} // namespace Sandbox
