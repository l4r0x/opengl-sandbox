#include "MeshRenderer.hpp"

namespace Sandbox {

MeshRenderer::MeshRenderer(std::shared_ptr<Shader> shader,
                           std::shared_ptr<Mesh> mesh,
                           std::shared_ptr<Texture> texture)
    : Spatial(), shader(shader), mesh(mesh), texture(texture) {}

void MeshRenderer::Render(float delta, const glm::mat4 &view,
                          const glm::mat4 &projection) const {
    texture->Bind();
    shader->Bind(projection, view, GetTransform().Model(),
                 GetTransform().NormalMatrix(view));
    shader->Diffuse(glm::vec3(0.2f, 0.4f, 0.4f), glm::vec3(1.0f, 1.0f, 1.0f),
                    0.4f);
    mesh->Draw();
    shader->Unbind();
    texture->Unbind();
}

} // namespace Sandbox
