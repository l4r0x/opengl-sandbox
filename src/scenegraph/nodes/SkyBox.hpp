#pragma once

#include <glm/glm.hpp>
#include <iostream>
#include <memory>
#include <string>

#include "rendering/Mesh.hpp"
#include "rendering/Shader.hpp"
#include "scenegraph/Node.hpp"
#include "Spatial.hpp"

namespace Sandbox {

/**
 * @brief      Class for rendering a sky.
 */
class SkyBox : public Node {
  private:
    glm::vec4 horizonColor;
    glm::vec4 skyColor;

    std::shared_ptr<Shader> shader;
    std::shared_ptr<Mesh> mesh;

  public:
    /**
     * @brief      Creates a new sky box.
     *
     * This sky box is a cube around the camera which is rendered behind all
     * other objects.
     *
     * @param[in]  horizonColor  The color of the horizon.
     * @param[in]  skyColor      The color of the top of the sky.
     * @param      shader        The shader for this sky box.
     */
    SkyBox(glm::vec4 horizonColor, glm::vec4 skyColor,
           std::shared_ptr<Shader> shader);

    void Render(float delta, const glm::mat4 &view,
                const glm::mat4 &projection) const override;
};

} // namespace Sandbox
