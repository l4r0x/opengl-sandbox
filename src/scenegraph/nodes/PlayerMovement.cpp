
#include "PlayerMovement.hpp"

#include <GLFW/glfw3.h>

#include <glm/gtc/constants.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/rotate_vector.hpp>

namespace Sandbox {

glm::vec3 const Direction::InputDir() const {
    glm::vec3 dir = glm::vec3((float)(right - left), (float)(up - down),
                              (float)(back - forward));
    return glm::normalize(dir);
}

PlayerMovement::PlayerMovement(float speed, float rotationSpeed)
    : Spatial(), speed(speed), rotationSpeed(rotationSpeed),
      movementAcceleration(4.0f), rotationAcceleration(3.0f), movementForce(),
      enableRotation(false), direction() {}

void PlayerMovement::UpdateMovementForce(float delta) {
    if (direction.NotZero()) {
        glm::vec3 dir = direction.InputDir();
        dir = glm::rotateY(dir, GetTransform().Rotation().y);

        movementForce += dir * movementAcceleration * delta;

        if (glm::length(movementForce) > 1.0f) {
            movementForce = glm::normalize(movementForce);
        }
    } else {
        if (glm::length(movementForce) > 2 * movementAcceleration * delta) {
            movementForce -=
                glm::normalize(movementForce) * movementAcceleration * delta;
        } else {
            movementForce = glm::vec3();
        }
    }
}

void PlayerMovement::Move(float delta) {
    GetTransformMut().SetPosition(GetTransform().Position() +
                                  movementForce * speed * delta);
}

void PlayerMovement::Rotate(float delta) {
    if (enableRotation) {
        float m = rotationSpeed * delta;
        glm::vec3 rotation = GetTransform().Rotation();

        rotation.x = glm::clamp(rotation.x + cursorOffset.y * m,
                                -glm::half_pi<float>(), glm::half_pi<float>());
        rotation.y = rotation.y + cursorOffset.x * m;

        GetTransformMut().SetRotation(rotation);
    }
    cursorOffset = glm::vec2();
}

void PlayerMovement::Update(float delta, bool updated,
                            const glm::mat4 &transform) {
    Rotate(delta);
    UpdateMovementForce(delta);
    Move(delta);

    Spatial::Update(delta, true, transform);
}

void PlayerMovement::OnEvent(Event &event) {
    if (event.GetType() == EventType::Keyboard) {
        KeyboardEvent &e = (KeyboardEvent &)event;
        int key = e.GetKeyCode();

        if (e.GetAction() == KeyAction::KeyPressed) {
            if (key == GLFW_KEY_A) direction.left = true;
            if (key == GLFW_KEY_D) direction.right = true;

            if (key == GLFW_KEY_LEFT_SHIFT) direction.down = true;
            if (key == GLFW_KEY_SPACE) direction.up = true;

            if (key == GLFW_KEY_W) direction.forward = true;
            if (key == GLFW_KEY_S) direction.back = true;
        } else if (e.GetAction() == KeyAction::KeyReleased) {
            if (key == GLFW_KEY_A) direction.left = false;
            if (key == GLFW_KEY_D) direction.right = false;

            if (key == GLFW_KEY_LEFT_SHIFT) direction.down = false;
            if (key == GLFW_KEY_SPACE) direction.up = false;

            if (key == GLFW_KEY_W) direction.forward = false;
            if (key == GLFW_KEY_S) direction.back = false;
        }

        if (e.GetKeyCode() == GLFW_KEY_LEFT_ALT) {
            enableRotation = e.GetAction() != KeyAction::KeyReleased;
        }
    } else if (event.GetType() == EventType::MouseButton) {
        MouseButtonEvent &e = (MouseButtonEvent &)event;
        if (e.GetButton() == 1) {
            enableRotation = e.GetAction() != KeyAction::KeyReleased;
        }
    } else if (event.GetType() == EventType::MouseMove) {
        MouseMoveEvent &e = (MouseMoveEvent &)event;
        cursorOffset += e.GetPosition() - recentCursorPos;
        recentCursorPos = e.GetPosition();
    }
    Node::OnEvent(event);
}

} // namespace Sandbox
