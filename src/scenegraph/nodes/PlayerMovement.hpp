#pragma once

#include <glm/glm.hpp>

#include "Spatial.hpp"

namespace Sandbox {

/**
 * @brief   Helper class for translating the user input to a three dimensional
 * direction.
 */
struct Direction {
    bool forward;
    bool back;
    bool up;
    bool down;
    bool left;
    bool right;

    Direction(bool forward = false, bool back = false, bool up = false,
              bool down = false, bool left = false, bool right = false)
        : forward(forward), back(back), up(up), down(down), left(left),
          right(right) {}

    inline bool NotZero() const {
        return right != left || up != down || back != forward;
    }

    glm::vec3 const InputDir() const;
};

/**
 * @brief      Handles the player movement inside the 3D world.
 *
 * This is a simple first person controller which normally has the camera
 * as direct child.
 */
class PlayerMovement : public Spatial {
  private:
    float speed;
    float rotationSpeed;

    float movementAcceleration;
    float rotationAcceleration;

    glm::vec3 movementForce;

    void UpdateMovementForce(float delta);
    void Move(float delta);
    void Rotate(float delta);

    glm::vec2 recentCursorPos;
    glm::vec2 cursorOffset;

  public:
    /**
     * Enables the rotation with the mouse cursor.
     */
    bool enableRotation;

    // Direction
    Direction direction;

    /**
     * @brief      Creates a new first person controller.
     *
     * @param[in]  speed          The movement speed.
     * @param[in]  rotationSpeed  The rotation speed.
     */
    PlayerMovement(float speed, float rotationSpeed);

    PlayerMovement(PlayerMovement const &) = delete;
    void operator=(PlayerMovement const &) = delete;

    void Update(float delta, bool updated = false,
                const glm::mat4 &transform = glm::mat4(1.0f)) override;

    void OnEvent(Event &event) override;
};

} // namespace Sandbox
