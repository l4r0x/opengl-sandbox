//------------------------------------------------------------------------------
// Window
//
// Wrapper for glfw windows in a single window application.
//------------------------------------------------------------------------------

#include "MainWindow.hpp"

#include <glad/glad.h>

namespace Sandbox {

MainWindow::MainWindow() : closing(false), enabledCursor(true) {
    // window hints
    glfwDefaultWindowHints();
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_SRGB_CAPABLE, GLFW_TRUE);

    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    window = glfwCreateWindow(WINDOW_WIDTH, WINDOW_HEIGHT, "Sandbox", nullptr,
                              nullptr);

    if (window == nullptr) {
        std::cerr << "Failed to create the GLFW window!" << std::endl;
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    // load OpenGL
    glfwMakeContextCurrent(window);
    if (!gladLoadGL()) {
        std::cerr << "Failed to load OpenGL!" << std::endl;
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    std::cout << "OpenGL " << glGetString(GL_VERSION) << std::endl;
    CursorMode(enabledCursor);

    // setup user pointer
    glfwSetWindowUserPointer(window, this);

    // initialize event callbacks
    InitCallbacks();

    // initialize ui
    ui = std::make_unique<UI>(this);

    // support HDPI
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);

    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glDisable(GL_SCISSOR_TEST);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    // delta time calculation
    renderTime = glfwGetTime();
    updateTime = glfwGetTime();
}

MainWindow::~MainWindow() {}

void MainWindow::Update() {
    // delta time
    float currentFrame = glfwGetTime();
    float delta = currentFrame - updateTime;
    updateTime = currentFrame;

    if (updateCallback)
        updateCallback(delta);

    glfwPollEvents();
}

void MainWindow::Render() {
    // delta time
    float currentFrame = glfwGetTime();
    float delta = currentFrame - renderTime;
    renderTime = currentFrame;

    // Rendering
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    if (renderCallback)
        renderCallback(delta);

    // user interface
    ui->NewFrame(delta);
    ui->DrawInfo();
    ui->DrawFps();
    ui->DrawCursorMode();
    ui->Render();
    glfwSwapBuffers(window);
}

void MainWindow::showAndWait() {
    while (!ShouldClose()) {
        Update();
        Render();
    }
    glfwDestroyWindow(window);
}

void MainWindow::OnEvent(Event &event) {
    ui->OnEvent(event);
    if (!event.handled) {
        eventCallback(event);
    }
}

void MainWindow::InitCallbacks() {
    glfwSetWindowCloseCallback(window, [](GLFWwindow *window) {
        MainWindow *win =
            static_cast<MainWindow *>(glfwGetWindowUserPointer(window));
        WindowCloseEvent event;
        win->OnEvent(event);
        win->Close();
    });

    glfwSetFramebufferSizeCallback(
        window, [](GLFWwindow *window, int width, int height) {
            glViewport(0, 0, width, height);

            MainWindow *win =
                static_cast<MainWindow *>(glfwGetWindowUserPointer(window));
            WindowResizeEvent event(width, height);
            win->OnEvent(event);
        });

    glfwSetKeyCallback(window, [](GLFWwindow *window, int key, int scancode,
                                  int action, int mode) {
        MainWindow *win =
            static_cast<MainWindow *>(glfwGetWindowUserPointer(window));

        switch (action) {
        case GLFW_PRESS: {
            KeyboardEvent event(key, KeyAction::KeyPressed);
            win->OnEvent(event);
            break;
        }
        case GLFW_RELEASE: {
            KeyboardEvent event(key, KeyAction::KeyReleased);
            win->OnEvent(event);
            break;
        }
        case GLFW_REPEAT: {
            KeyboardEvent event(key, KeyAction::KeyRepeated);
            win->OnEvent(event);
            break;
        }
        }
    });

    glfwSetCursorPosCallback(
        window, [](GLFWwindow *window, double xpos, double ypos) {
            MainWindow *win =
                static_cast<MainWindow *>(glfwGetWindowUserPointer(window));
            MouseMoveEvent event(glm::vec2(xpos, ypos));
            win->OnEvent(event);
        });

    glfwSetMouseButtonCallback(
        window, [](GLFWwindow *window, int button, int action, int mods) {
            MainWindow *win =
                static_cast<MainWindow *>(glfwGetWindowUserPointer(window));

            switch (action) {
            case GLFW_PRESS: {
                MouseButtonEvent event(button, KeyAction::KeyPressed);
                win->OnEvent(event);
                break;
            }
            case GLFW_RELEASE: {
                MouseButtonEvent event(button, KeyAction::KeyReleased);
                win->OnEvent(event);
                break;
            }
            }
        });

    glfwSetScrollCallback(
        window, [](GLFWwindow *window, double xoffset, double yoffset) {
            MainWindow *win =
                static_cast<MainWindow *>(glfwGetWindowUserPointer(window));
            MouseScrollEvent event(glm::vec2(xoffset, yoffset));
            win->OnEvent(event);
        });

    glfwSetCharCallback(window, [](GLFWwindow *window, unsigned int c) {
        MainWindow *win =
            static_cast<MainWindow *>(glfwGetWindowUserPointer(window));
        KeyCharEvent event(c);
        win->OnEvent(event);
    });
}

glm::vec2 MainWindow::ViewportSize() const {
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    return glm::vec2(width, height);
}

float MainWindow::ViewportAspect() const {
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    return ((float)width) / height;
}

glm::vec2 MainWindow::WindowSize() const {
    int width, height;
    glfwGetWindowSize(window, &width, &height);
    return glm::vec2(width, height);
}

void MainWindow::Close() { closing = true; }

} // namespace Sandbox
